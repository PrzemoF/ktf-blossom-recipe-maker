<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * The template loader of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Blossom_Recipe
 * @subpackage Blossom_Recipe/admin
 * @author     Blossom <test@test.com>
 */
class Blossom_Recipe_Maker_Functions {

	/**
	 * Hook in methods.
	 */
	function pagination_bar( $custom_query ) {
	    $total_pages = $custom_query->max_num_pages;
	    $big = 999999999; // need an unlikely integer

	    if ($total_pages > 1){
	        $current_page = max(1, get_query_var('paged'));

	        echo '<nav class="navigation pagination" role="navigation"><h2 class="screen-reader-text">Posts navigation</h2><div class="nav-links">';

	        echo paginate_links(array(
	            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	            'format' => '?paged=%#%',
	            'current' => $current_page,
	            'total' => $total_pages,
	            'prev_text' => __( 'Previous', 'blossom-recipe-maker' ),
               	'next_text' => __( 'Next', 'blossom-recipe-maker' ),
	        ));

	        echo "</div></nav>\n";
	    }
	}
	
	public static function difficulty_levels()
	{
		$levels = array(
				'Easy' => __('Easy', 'blossom-recipe-maker'),
				'Medium' => __('Medium', 'blossom-recipe-maker'),
				'Difficult' => __('Difficult', 'blossom-recipe-maker')
			);

		$levels = apply_filters( 'br_recipe_difficulty_level_options', $levels );

		return $levels;
	}

	/**
	 * Get difficulty Label
	*/
	public static function get_difficulty_label( $difficulty ) 
	{
	    $levels = self::difficulty_levels();

	    $difficulty = ($difficulty) ? $levels[$difficulty] : $difficulty;

		return $difficulty;
	}

	public static function measurements()
	{

		// Use the "br_recipe_measurement_units" filter to add your own measurements.
		$measurements = array(
			'g' => _nx_noop('gram', 'grams', 'Measurement unit', 'blossom-recipe-maker'),
			'kg' =>_nx_noop('kilogram', 'kilograms', 'Measurement unit', 'blossom-recipe-maker'),
			'oz' => _nx_noop( 'ounce', 'ounces', 'Measurement unit', 'blossom-recipe-maker'),
			'floz' => _nx_noop('fluid ounce', 'fluid ounces', 'Measurement unit', 'blossom-recipe-maker'),
			'cup' => _nx_noop('cup', 'cups', 'Measurement unit', 'blossom-recipe-maker'),
			'tsp' => _nx_noop('teaspoon', 'teaspoons', 'Measurement unit', 'blossom-recipe-maker'),
			'tbsp' => _nx_noop('tablespoon', 'tablespoons', 'Measurement unit', 'blossom-recipe-maker'),
			'ml' => _nx_noop('milliliter', 'milliliters', 'Measurement unit', 'blossom-recipe-maker'),
			'l' => _nx_noop('liter', 'liters', 'Measurement unit', 'blossom-recipe-maker'),
			'stick' => _nx_noop('stick', 'sticks', 'Measurement unit', 'blossom-recipe-maker'),
			'lb' => _nx_noop('pound', 'pounds', 'Measurement unit', 'blossom-recipe-maker'),
			'dash' => _nx_noop('dash', 'dashes', 'Measurement unit', 'blossom-recipe-maker'),
			'drop' => _nx_noop('drop', 'drops', 'Measurement unit', 'blossom-recipe-maker'),
			'gal' => _nx_noop('gallon', 'gallons', 'Measurement unit', 'blossom-recipe-maker'),
			'pinch' => _nx_noop('pinch', 'pinches', 'Measurement unit', 'blossom-recipe-maker'),
			'pt' => _nx_noop('pint', 'pints', 'Measurement unit', 'blossom-recipe-maker'),
			'qt' => _nx_noop('quart', 'quarts', 'Measurement unit', 'blossom-recipe-maker'),
		);

		$measurements = apply_filters( 'br_recipe_measurement_units', $measurements );

		return $measurements;

	}

	public static function time_format( $minutes, $format ){

		ob_start();

		if ( $minutes < 60 ):
			if ( $format === 'iso' ):
				return 'PT0H'.$minutes.'M';
			endif;

		elseif ( $minutes < 1440 ):
			$hours = floor( $minutes / 60 );
			$minutes_left = $minutes - ( $hours * 60 );
			if ( $format === 'iso' ):
				return 'PT'.$hours.'H'.( $minutes_left ? $minutes_left : 0 ).'M';
			endif;

		else:
			$days = floor( $minutes / 24 / 60 );
			$minutes_left = $minutes - ( $days * 24 * 60 );
			if ( $minutes_left > 60 ):
				$hours_left = floor( $minutes_left / 60 );
				$minutes_left = $minutes_left - ( $hours_left * 60 );
			endif;
			if ( $format === 'iso' ):
				return 'P'.$days.'DT'.( $hours_left ? $hours_left : 0 ).'H'.( $minutes_left ? $minutes_left : 0 ).'M';
			endif;

		endif;

		return ob_get_clean();

	}

	function brm_posted_on( $icon = false ) {
    
        echo '<span class="posted-on">';
        
        if( $icon ) echo '<i class="fa fa-calendar" aria-hidden="true"></i>';
        
        printf( '<a href="%1$s" rel="bookmark"><time class="entry-date published updated" datetime="%2$s">%3$s</time></a>', esc_url( get_permalink() ), esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) );
        
        echo '</span>';

    }

    function brm_minify_css( $input ) 
    {
      if(trim($input) === "") return $input;
      // Force white-space(s) in `calc()`
      if(strpos($input, 'calc(') !== false) {
          $input = preg_replace_callback('#(?<=[\s:])calc\(\s*(.*?)\s*\)#', function($matches) {
              return 'calc(' . preg_replace('#\s+#', "\x1A", $matches[1]) . ')';
          }, $input);
      }
      return preg_replace(
          array(
              // Remove comment(s)
              '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
              // Remove unused white-space(s)
              '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~+]|\s*+-(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
              // Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
              '#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
              // Replace `:0 0 0 0` with `:0`
              '#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
              // Replace `background-position:0` with `background-position:0 0`
              '#(background-position):0(?=[;\}])#si',
              // Replace `0.6` with `.6`, but only when preceded by a white-space or `=`, `:`, `,`, `(`, `-`
              '#(?<=[\s=:,\(\-]|&\#32;)0+\.(\d+)#s',
              // Minify string value
              '#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][-\w]*?)\2(?=[\s\{\}\];,])#si',
              '#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
              // Minify HEX color code
              '#(?<=[\s=:,\(]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
              // Replace `(border|outline):none` with `(border|outline):0`
              '#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
              // Remove empty selector(s)
              '#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s',
              '#\x1A#'
          ),
          array(
              '$1',
              '$1$2$3$4$5$6$7',
              '$1',
              ':0',
              '$1:0 0',
              '.$1',
              '$1$3',
              '$1$2$4$5',
              '$1$2$3',
              '$1:0',
              '$1$2',
              ' '
          ),
      $input);
  }

    function brm_get_fallback_svg( $post_thumbnail ) {
	    if( ! $post_thumbnail ){
	        return;
	    }
	    
		$image_size = self::brm_get_image_sizes( $post_thumbnail );
		$svg_fill   = apply_filters('brm_fallback_svg_fill', 'fill:#f2f2f2;');
	     
	    if( $image_size ){ ?>	        
            <svg class="fallback-svg" viewBox="0 0 <?php echo esc_attr( $image_size['width'] ); ?> <?php echo esc_attr( $image_size['height'] ); ?>" preserveAspectRatio="none">
                    <rect width="<?php echo esc_attr( $image_size['width'] ); ?>" height="<?php echo esc_attr( $image_size['height'] ); ?>" style="<?php echo $svg_fill;?>"></rect>
            </svg>
	        <?php
	    }
	}

	function brm_get_image_sizes( $size = '' ) {
 
	    global $_wp_additional_image_sizes;
	 
	    $sizes = array();
	    $get_intermediate_image_sizes = get_intermediate_image_sizes();
	 
	    // Create the full array with sizes and crop info
	    foreach( $get_intermediate_image_sizes as $_size ) {
	        if ( in_array( $_size, array( 'thumbnail', 'medium', 'medium_large', 'large' ) ) ) {
	            $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
	            $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
	            $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );
	        } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
	            $sizes[ $_size ] = array( 
	                'width' => $_wp_additional_image_sizes[ $_size ]['width'],
	                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
	                'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
	            );
	        }
	    } 
	    // Get only 1 size if found
	    if ( $size ) {
	        if( isset( $sizes[ $size ] ) ) {
	            return $sizes[ $size ];
	        } else {
	            return false;
	        }
	    }

	    return $sizes;
	}


}
new Blossom_Recipe_Maker_Functions();
